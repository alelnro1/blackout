<?php
/**
 * Created by PhpStorm.
 * User: Julian
 * Date: 17/9/2017
 * Time: 23:44
 */

namespace App\Clases;


class Calculos
{

    public static function hayInterseccion($coordenada1, $radio1, $coordenada2, $radio2)
    {
        $distancia_centros = Calculos::distancia($coordenada1, $coordenada2);
        $suma_radios = $radio1 + $radio2;
        return ($distancia_centros < $suma_radios) ? true : false;

    }

    /**
     * @param $coordenada
     * @return array
     * co[0] -> latitud
     * co[1] -> longitud
     */
    public static function formatearCoordenadaAFloat($coordenada)
    {

        $co = explode(";", $coordenada);
        $co[0] = (float)substr(preg_replace('/[^0-9_._-]/', '', trim($co[0])), 0, 11);
        $co[1] = (float)substr(preg_replace('/[^0-9_._-]/', '', trim($co[1])), 0, 11);
        return $co;
    }

    public static function distancia($coordenada1, $coordenada2, $unit = "m")
    {

        $coordenada1 = static::formatearCoordenadaAFloat($coordenada1);
        $coordenada2 = static::formatearCoordenadaAFloat($coordenada2);

        $lat1 = $coordenada1[0];
        $lon1 = $coordenada1[1];
        $lat2 = $coordenada2[0];
        $lon2 = $coordenada2[1];

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);


        $distancia = ($miles * 1609.344);

        return $distancia;
    }

}