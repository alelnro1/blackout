<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comentario extends Model
{
    protected $table = "comentarios";

    protected $fillable = [
        'user_id', 'empresa_id', 'descripcion'
    ];
}
