<?php

namespace App\Console;

use App\Corte;
use App\Reporte;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Api\ReportesController;
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        $schedule->call(function() {
            $ahora = Carbon::now();

            /* Damos de baja todos los cortes cuya fecha fin es mayor a ahora*/
            Corte::where('fecha_fin', '<', $ahora)
                    ->where('es_programado', true)
                    ->where('resuelto', false)
                    ->get()
                    ->each(function ($corte) use ($ahora) {
                        $corte->resuelto = true;
                        $corte->save();

                        Log::info("Corte actualizado por fecha fin: " . $corte->id);
                    });

            /* Damos de baja todos los reportes que su ultimo updated_at sea > 25 horas */
            Reporte::where('resuelto', '<>', true)
                ->get()
                ->each(function ($reporte) use ($ahora) {
                    $horas_desde_ult_actualizacion = abs($ahora->diffInMinutes($reporte->updated_at));

                    if ($horas_desde_ult_actualizacion >= 1500) {
                        Log::info("Reporte actualizado " . $reporte->id . " con diferencia de horas: " . $horas_desde_ult_actualizacion);

                        (new ReportesController)->resolver($reporte);
                    }
                });
        })->everyMinute();

         /*$schedule->command('inspire')
                  ->hourly();*/
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
