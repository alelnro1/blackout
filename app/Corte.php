<?php

namespace App;

use App\Http\Controllers\Api\CortesController;
use Illuminate\Database\Eloquent\Model;
use App\Clases\Calculos;
use App\Reporte;
use Illuminate\Support\Facades\DB;

class Corte extends Model
{
    protected $table = "cortes";

    protected $fillable = [
        'empresa_id',
        'ubicacion',
        'radio',
        'cantidad_reportes',
        'duracion',
        'resuelto',
        'fecha_inicio',
        'fecha_fin',
        'servicio_id'
    ];

    public function Empresa()
    {
        return $this->belongsTo(Empresa::class, 'empresa_id');
    }

    public function Reportes()
    {
        return $this->hasMany(Reporte::class);
    }

    public function Servicio()
    {
        return $this->belongsTo(Servicio::class, 'servicio_id');
    }

    public function resolverUnReporte($reporte)
    {
        $this->cantidad_reportes = (new Reporte)->getCantidadReportesDeCorte($this->id);
        $this->save();
        //if ($this->cantidad_reportes < env('CANTDAD_DE_INTERSECCIONES')) {
        if ($this->cantidad_reportes < 3) {
            $reportes_a_resolver_automatico = Reporte::where('corte_id', $reporte->corte_id)->where('empresa_id', $reporte->empresa_id)->where('resuelto', 0)->get();
            foreach ($reportes_a_resolver_automatico as $reporte_a_resolver) {
                $reporte_a_resolver->resuelto = 1;
                $reporte_a_resolver->save();
            }
            $this->resuelto = true;
            $this->fecha_fin = date("Y-m-d H:i:s");
            $this->cantidad_reportes = 0;
            $this->save();
        } else {
            CortesController::centrarRedimensionarCorte($this);
        }
        return $this;
    }


    public static function getCortesParaInterseccion($reporte)
    {
        return static::where('servicio_id', $reporte->servicio_id)->where('empresa_id', $reporte->empresa_id)->where('resuelto', 0)->where('es_programado', null)->get();
    }


    public function asociarReporte($reporte)
    {
        $reporte->corte_id = $this->id;
        $reporte->save();
        $this->cantidad_reportes = (new Reporte)->getCantidadReportesDeCorte($this->id);
        $this->save();
        CortesController::centrarRedimensionarCorte($this);
        return true;
    }


    public function crearNuevoCorte($nuevo_reporte, $reportesFundadores)
    {
        $corte = Corte::create([
            'servicio_id' => $nuevo_reporte->servicio_id,
            'empresa_id' => $nuevo_reporte->empresa_id,
            'fecha_inicio' => $nuevo_reporte->created_at,
            'ubicacion' => $nuevo_reporte->ubicacion,
            'radio' => $nuevo_reporte->radio
        ]);

        $nuevo_reporte->corte_id = $corte->id;
        $nuevo_reporte->save();
        //asocio cortes y reportes y ademas recalculo centro del corte
        foreach ($reportesFundadores as $reporteFundador) {
            $corte->asociarReporte($reporteFundador);
        }
        return true;

    }
}
