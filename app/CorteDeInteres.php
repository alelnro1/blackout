<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CorteDeInteres extends Model
{
    protected $table = "cortes_de_interes";

    protected $fillable = [
        'user_id', 'corte_id', 'es_programado'
    ];

    public $timestamps = false;

    public function User()
    {
        return $this->belongsTo(User::class);
    }

    public function Corte()
    {
        return $this->belongsTo(Corte::class);
    }
}
