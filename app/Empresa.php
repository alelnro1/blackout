<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $table = "empresas";

    protected $fillable = [
        'nombre',
        'direccion',
        'website',
        'telefono',
        'email',
        'email_contacto',
        'password'
    ];

    public function Servicios()
    {
        return $this->belongsToMany(Servicio::class, 'empresa_servicio', 'empresa_id', 'servicio_id');
    }
}
