<?php

namespace App\Http\Controllers\Api\Auth;

use App\Empresa;
use App\Http\Controllers\Api\Traits\IssueTokenTrait;
use App\Http\Requests\PersonaRequest;
use App\Http\Requests\RegisterRequest;
use App\Persona;
use App\Servicio;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;
use Laravel\Passport\Client;

class RegisterController extends Controller
{
    use IssueTokenTrait;

    private $client;

    public function __construct()
    {
        $this->client = Client::find(1);
    }

    /**
     * Se registra un usuario-persona
     *
     * @param Request $request
     * @return mixed
     */
    public function register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed'
        ]);

        // Registro primero a la persona
        $persona = $this->crearPersona($request);

        // Todo bien => Registramos el usuario
        $user = User::create([
            'name' => request('name'),
            'email' => request('email'),
            'password' => bcrypt(request('password')),
            'persona_id' => $persona->id
        ]);

        return $this->issueToken($request, 'password');
    }

    /**
     * Se registra un usuario-empresa
     *
     * @param Request $request
     * @return mixed
     */
    public function registerEmpresa(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'direccion' => 'required',
            'website' => 'required',
            'telefono' => 'required',

            'email' => 'required|string|email|max:255|unique:users',
            'email_contacto' => 'required|string',
            'password' => 'required|string|min:6|confirmed',

            'servicio_id' => 'required|integer'
        ]);

        // Registro primero a la persona
        $empresa = $this->crearEmpresa($request);

        // Todo bien => Registramos el usuario
        $user = User::create([
            'name' => request('name'),
            'email' => request('email'),
            'email_contacto' => request('email_contacto'),
            'password' => bcrypt(request('password')),
            'empresa_id' => $empresa->id
        ]);

        // Asociamos el servicio
        $servicio = Servicio::findOrFail($request->servicio_id);
        $empresa->Servicios()->attach($servicio);

        return $this->issueToken($request, 'password');
    }

    /**
     * Se está registrando un usuario => creo la persona con los datos del request
     *
     * @param $request
     * @return $this|\Illuminate\Database\Eloquent\Model
     */
    private function crearPersona($request)
    {
        $persona =
            Persona::create([
                'name' => $request->name,
                'email' => $request->email
            ]);

        return $persona;
    }

    /**
     * Se está registrando un usuario-empresa => creo la empresa con los datos del request
     *
     * @param $request
     * @return $this|\Illuminate\Database\Eloquent\Model
     */
    private function crearEmpresa($request)
    {
        $empresa = Empresa::create([
            'nombre' => $request->name,
            'direccion' => $request->direccion,
            'website' => $request->website,
            'telefono' => $request->telefono,
            'email' => $request->email,
            'email_contacto' => $request->email_contacto
        ]);

        return $empresa;
    }
}
