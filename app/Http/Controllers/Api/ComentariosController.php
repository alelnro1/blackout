<?php

namespace App\Http\Controllers\Api;

use App\Comentario;
use App\Http\Controllers\Controller;
use App\Http\Requests\ComentarioRequest;
use Illuminate\Http\Request;

class ComentariosController extends Controller
{
    /**
     * Agrego un comentario a una empresa. Uno por usuario. Si existe se actualiza
     *
     * @param ComentarioRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function agregar(ComentarioRequest $request)
    {
        $comentario =
            Comentario::updateOrCreate(
                // Valores a buscar
                [
                    'user_id' => $request->user_id,
                    'empresa_id' => $request->empresa_id
                ],

                // Valores a actualizar
                [
                    'descripcion' => $request->descripcion
                ]
            );

        return response()->json([
            'comentario' => $comentario
        ]);
    }

    /**
     * Muestro el comentario de un usuario para una empresa
     *
     * @param Comentario $comentario
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Comentario $comentario)
    {
        return response()->json([
            'comentario' => $comentario
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comentario $comentario)
    {
        $comentario = $comentario->delete();

        return response()->json($comentario, 200);
    }

    /**
     * Obtengo todas las sucursales de una empresa en especifico
     *
     * @param $empresa_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getComentariosDeEmpresa($empresa_id)
    {
        $comentarios =
            Comentario::where('empresa_id', $empresa_id)
                ->orderBy('updated_at', 'DESC')
                ->get();

        return response()->json($comentarios, 200);
    }
}
