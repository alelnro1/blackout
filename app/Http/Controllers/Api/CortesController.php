<?php

namespace App\Http\Controllers\Api;

use App\Corte;
use App\Empresa;
use App\Http\Requests\CorteProgramadoRequest;
use App\Respuesta;
use App\Reporte;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use phpDocumentor\Reflection\Types\This;
use App\Clases\Calculos;

class CortesController extends Controller
{
    public function index()
    {
        $cortes = Corte::with([
            'Empresa',
            'Servicio'
        ])->get();

        return response()->json([
            'cortes' => $cortes
        ], 200);
    }

    /**
     * Damos de alta un corte programado
     *
     * @param CorteProgramadoRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function altaCorteProgramado(CorteProgramadoRequest $request)
    {
        $corte = Corte::create($request->all());

        $corte->es_programado = true;
        $corte->resuelto = false;
        $corte->save();

        return response()->json($corte, 200);
    }

    public function updateCorteProgramado(Corte $corte)
    {
        //$corte->update($re)
    }

    /**
     * Se resuelve un corte programado
     *
     * @param Corte $corte
     * @return \Illuminate\Http\JsonResponse
     */
    public function resolverCorteProgramado(Corte $corte)
    {
        $corte->resuelto = true;

        $corte->save();

        return response()->json($corte, 200);
    }

    /**
     * Se elimina un corte programado
     *
     * @param Corte $corte
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroyCorteProgramado(Corte $corte)
    {
        $corte->delete();

        return response()->json($corte, 200);
    }

    /**
     * Obtengo todos los cortes de una empresa en especifico
     *
     * @param $empresa_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCortesDeEmpresa(Empresa $empresa)
    {
        $cortes =
            Corte::where('empresa_id', $empresa->id)
                ->get();

        return response()->json($cortes, 200);
    }


    /**
     * Obtengo las respuestas de un corte
     *
     * @param Corte $corte
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRespuestasDeCorte(Corte $corte)
    {
        $respuestas =
            Respuesta::where('corte_id', $corte->id)
                ->orderBy('updated_at', 'DESC')
                ->get();

        return response()->json($respuestas, 200);
    }

    public static function actualizarReportesDeCorte($reporte)
    {
        $corte = Corte::find($reporte->corte_id);
        $corte->resolverUnReporte($reporte);
    }


    public static function AgregarReporteACorteNuevo($nuevo_reporte)
    {
        $reportesFundadores = [];
        $interseccion = false;
        $cantidad_intersecciones = 0;
        $reportes = Reporte::getReportesParaInterseccion($nuevo_reporte);

        foreach ($reportes as $reporte) {
            $interseccion = Calculos::hayInterseccion($reporte->ubicacion, $reporte->radio, $nuevo_reporte->ubicacion, $nuevo_reporte->radio);
            if ($interseccion) {
                $cantidad_intersecciones = $cantidad_intersecciones + 1;
                array_push($reportesFundadores, $reporte);
            }
        }

        //echo "cantidad_intersecciones: " .$cantidad_intersecciones . '<br>';
        //if ($cantidad_intersecciones >= env('CANTDAD_DE_INTERSECCIONES')) {
        if ($cantidad_intersecciones >= 3) {
            //Generar Nuevo Corte
            (new Corte)->crearNuevoCorte($nuevo_reporte, $reportesFundadores);
            return true;
        }
        return false;
    }

    public static function AgregarReporteACorteExistente($nuevo_reporte)
    {

        $interseccion = false;
        $cortes = Corte::getCortesParaInterseccion($nuevo_reporte);
        foreach ($cortes as $corte) {
            if (Calculos::hayInterseccion($corte->ubicacion, $corte->radio, $nuevo_reporte->ubicacion, $nuevo_reporte->radio)) {
                //asociar reporte a corte
                return $corte->asociarReporte($nuevo_reporte);
            }
        }
        return false;
    }

    // Esta función se ecarga de asociar el reporte a un corte existente, a uno nuevo, o no hacer nada.
    public static function administraReporte($nuevo_reporte)
    {   //Devuelve True si asocio un reporte a un corte ya sea un nuevo corte o un corte existente.
        //Devuelve False si no hizo nada.

        //si no se agrego el reporte a ningun corte, valido si este nuevo reporte no desencadena la creacion de un nuevo corte
        return (static::AgregarReporteACorteExistente($nuevo_reporte)) ? true : static::AgregarReporteACorteNuevo($nuevo_reporte);

    }

    public static function centrarRedimensionarCorte($corte)
    {
        //Inicializo variables
        $reportes = $corte->Reportes;
        $cantidad_reportes = count($reportes);
        $suma_latitudes = 0;
        $suma_longitudes = 0;
        $distancia_mas_lejana = -1;

        //Recorro reportes del corte para sacar un promedio de la ubicacion
        foreach ($reportes as $reporte) {
            $coordenada_reporte = Calculos::formatearCoordenadaAFloat($reporte->ubicacion);
            $suma_latitudes = $suma_latitudes + $coordenada_reporte[0];
            $suma_longitudes = $suma_longitudes + $coordenada_reporte[1];
        }
        $coordenada[0] = $suma_latitudes / $cantidad_reportes;
        $coordenada[1] = $suma_longitudes / $cantidad_reportes;
        //Grabo la ubicacion que resulto del promedio de todos los cortes.
        $corte->ubicacion = implode(';', $coordenada);
        $corte->save();

        //Busco el radio maximo para asignarlo al corte.
        foreach ($reportes as $reporte) {
            $distancia_corte_reporte = Calculos::distancia($corte->ubicacion, $reporte->ubicacion) + $reporte->radio;
            //dump($distancia_corte_reporte);
            $distancia_mas_lejana = ($distancia_mas_lejana >= $distancia_corte_reporte) ? $distancia_mas_lejana : $distancia_corte_reporte;
        }
        //dump($distancia_mas_lejana);
        $corte->radio = $distancia_mas_lejana;
        $corte->save();
        return $corte;
    }
}
