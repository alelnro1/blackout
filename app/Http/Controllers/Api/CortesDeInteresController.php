<?php

namespace App\Http\Controllers\Api;

use App\CorteDeInteres;
use App\Http\Controllers\Controller;
use App\Http\Requests\CorteDeInteresRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CortesDeInteresController extends Controller
{
    /**
     * Alta de corte de interes
     *
     * @param CorteDeInteresRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CorteDeInteresRequest $request)
    {
        $corte =
            CorteDeInteres::create([
                'corte_id' => $request->corte_id,
                'user_id' => $request->user_id,
            ]);

        return response()->json($corte, 200);
    }

    /**
     * Obtengo los cortes de interes de un usuario
     *
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCortesDeInteresDeUsuario(User $usuario)
    {
        $cortes_de_interes = CorteDeInteres::where('user_id', $usuario->id)->get();

        return response()->json($cortes_de_interes, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(CorteDeInteres $corte)
    {
        $corte->delete();

        return response()->json([], 200);
    }
}
