<?php

namespace App\Http\Controllers\Api;

use App\Empresa;
use App\Http\Controllers\Controller;
use App\Http\Requests\EmpresaRequest;
use App\Servicio;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EmpresasController extends Controller
{
    /**
     * Obtengo todas las empresas del sistema
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $empresas = Empresa::with(['servicios'])->get();

        return response()->json(
            $empresas
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmpresaRequest $request)
    {
        $empresa = Empresa::create([
            'nombre' => request('nombre'),
            'direccion' => request('direccion'),
            'website' => request('website'),
            'telefono' => request('telefono'),
            'email' => request('email'),
            'email_contacto' => request('email_contacto')
        ]);

        if (isset($request->servicio_id)) {
            // Asocio el servicio recibido
            $servicio = Servicio::findOrFail($request->servicio_id);
            $empresa->Servicios()->attach($servicio);
        }

        $empresa = $empresa->load('Servicios');

        return response()->json([
            'empresa' => $empresa
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Empresa $empresa)
    {
        return response()->json([
            'empresa' => $empresa
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EmpresaRequest $request, $id)
    {
        // Busco la empresa
        $empresa = Empresa::findOrFail($id);

        // La actualizo con los datos que recibi
        $empresa->update($request->all());

        // Vuelvo a buscar la empresa para devolverla actualizada
        $empresa = Empresa::find($id);

        return response()->json([
            'empresa' => $empresa
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Empresa $empresa)
    {
        $empresa->delete();

        return response()->json([], 200);
    }

    public function habilitarEmpresa(Empresa $empresa)
    {
        $empresa->habilitada = true;
		$empresa->save();
        
        return response()->json($empresa);
    }

    public function deshabilitarEmpresa(Empresa $empresa)
    {
        $empresa->habilitada = false;
        $empresa->save();

        return response()->json($empresa);
    }
}
