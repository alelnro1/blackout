<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\PuntoDeInteresRequest;
use App\Persona;
use App\PuntoDeInteres;
use App\User;
use Illuminate\Http\Request;

class PuntosDeInteresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PuntoDeInteresRequest $request)
    {
        $punto_de_interes = PuntoDeInteres::create([
            'user_id' => $request->user_id,
            'empresa_id' => $request->empresa_id,
            'servicio_id' => $request->servicio_id,
            'ubicacion' => $request->ubicacion,
            'radio' => $request->radio
        ]);

        return response()->json([
            'punto_de_interes' => $punto_de_interes
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(PuntoDeInteres $punto)
    {

        return response()->json([
            'punto_de_interes' => $punto
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PuntoDeInteres $punto)
    {
        $punto->delete();

        return response()->json([], 200);
    }

    public function getPuntosDeInteresDeUsuario(User $usuario)
    {
        $puntos = PuntoDeInteres::where('user_id', $usuario->id)->get();

        return response()->json($puntos, 200);
    }
}
