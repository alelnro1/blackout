<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\ReporteRequest;
use App\Reporte;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use SebastianBergmann\CodeCoverage\Report\Xml\Report;
use App\Http\Controllers\Api\CortesController;

class ReportesController extends Controller
{
    /**
     * Obtengo todas las empresas del sistema
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $reportes = Reporte::where('resuelto', false)->get();

        return response()->json([
            'reportes', $reportes
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReporteRequest $request)
    {
        $reporte = Reporte::create([
            'user_id' => $request->user_id,
            'ubicacion' => $request->ubicacion,
            'radio' => $request->radio,
            'servicio_id' => $request->servicio_id,
            'empresa_id' => $request->empresa_id,
            'resuelto' => false
        ]);

        CortesController::administraReporte($reporte);

        return response()->json([
            'reporte' => $reporte
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Reporte $reporte)
    {
        return response()->json([
            'reporte' => $reporte
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reporte $reporte)
    {
        $reporte->delete();

        return response()->json([], 200);
    }

    /**
     * Se resuelve un reporte
     *
     * @param Reporte $reporte
     * @return \Illuminate\Http\JsonResponse
     */
    public function resolver(Reporte $reporte)
    {
        $reporte->resuelto = true;
        $reporte->save();
        ($reporte->corte_id != null) ? CortesController::actualizarReportesDeCorte($reporte) : false;
        return response()->json($reporte, 200);
    }

    /**
     * Obtengo todos los reportes por usuario
     *
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function getReportesDeUsuario(User $usuario)
    {
        $reportes = Reporte::where('user_id', $usuario->id)->get();

        return response()->json($reportes, 200);
    }

    /**
     * Se actualiza el campo updated_at de un reporte
     *
     * @param Reporte $reporte
     * @return \Illuminate\Http\JsonResponse
     */
    public function actualizarUpdatedAt(Reporte $reporte)
    {
        $reporte->updated_at = date("Y-m-d H:i:s", time());
        $reporte->save();

        return response()->json($reporte);
    }
}
