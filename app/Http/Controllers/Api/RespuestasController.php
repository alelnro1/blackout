<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\RespuestaRequest;
use App\Respuesta;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RespuestasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function agregar(RespuestaRequest $request)
    {
        $respuesta =
            Respuesta::updateOrCreate(
            // Valores a buscar
                [
                    'user_id' => $request->user_id,
                    'corte_id' => $request->corte_id
                ],

                // Valores a actualizar
                [
                    'descripcion' => $request->descripcion
                ]
            );

        return response()->json($respuesta);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Respuesta $respuesta)
    {
        return response()->json($respuesta, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Respuesta $respuesta)
    {
        $respuesta = $respuesta->delete();

        return response()->json($respuesta, 200);
    }
}
