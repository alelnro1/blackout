<?php

namespace App\Http\Controllers\Api;

use App\Empresa;
use App\Http\Controllers\Controller;
use App\Http\Requests\SucursalesRequest;
use App\Sucursal;
use Illuminate\Http\Request;

class SucursalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SucursalesRequest $request)
    {
        $sucursal = Sucursal::create([
            'empresa_id' => $request->empresa_id,
            'telefono' => $request->telefono,
            'direccion' => $request->direccion,
            'ubicacion' => $request->ubicacion,
        ]);

        return response()->json([
            'sucursal' => $sucursal
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Sucursal $sucursal)
    {
        return response()->json([
            'sucursal' => $sucursal
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sucursal $sucursal)
    {
        $sucursal->delete();

        return response()->json([], 200);
    }

    /**
     * Obtengo todas las sucursales de una empresa en especifico
     *
     * @param $empresa_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSucursalesDeEmpresa($empresa_id)
    {
        $sucursales = Sucursal::where('empresa_id', $empresa_id)->get();

        return response()->json($sucursales, 200);
    }
}
