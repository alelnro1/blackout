<?php

namespace App\Http\Controllers\Api;

use App\Empresa;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UsuariosController extends Controller
{
    /**
     * Obtengo todos los usuarios del sistema
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $usuarios = User::where('id','>', 0)->withTrashed()->get();

        return response()->json($usuarios, 200);
    }


    /**
     * Obtengo los datos del usuario logueado
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function mostrarUsuario(Request $request)
    {
        $usuario = $request->user();

        return response()->json([
            'id' => $usuario->id,
            'nombre' => $usuario->name,
            'email' => $usuario->email,
            'persona_id' => $usuario->persona_id,
            'empresa_id' => $usuario->empresa_id
        ], 200);
    }

    /**
     * @param Request $request
     */
    public function actualizar(Request $request)
    {
        $user = Auth::user();

        if ($request->password != null)
            $user->password = bcrypt($request->password);

        $user->name = $request->name ?: $user->name;
        $user->email = $request->email ?: $user->email;
        $user->save();

        return response()->json([
            'user' => $user
        ], 200);
    }

    /**
     * Creamos un usuario y lo asociamos con la empresa
     *
     * @param Empresa $empresa
     * @param Request $request
     */
    public function asociarEmpresaConUsuario(Empresa $empresa, Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed'
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);

        $user->empresa_id = $empresa->id;
        $user->save();

        return response()->json($user);
    }

    public function deshabilitar()
    {
        $user = Auth::user();

        $user->delete();

        return response()->json();
    }
}
