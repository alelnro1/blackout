<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CorteProgramadoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ubicacion' => 'required|string',
            'radio' => 'required|numeric',
            'empresa_id' => 'required|exists:empresas,id',
            'servicio_id' => 'required|exists:servicios,id',
            'fecha_inicio' => 'required|date_format:Y-m-d H:i|after:today',
            'fecha_fin' => 'required|date_format:Y-m-d H:i'
        ];
    }
}
