<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PuntoDeInteresRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required|exists:users,id',
            'empresa_id' => 'exists:empresas,id',
            'servicio_id' => 'exists:servicios,id',
            'ubicacion' => 'required',
            'radio' => 'required',
        ];
    }
}
