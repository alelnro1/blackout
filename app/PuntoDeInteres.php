<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PuntoDeInteres extends Model
{
    protected $table = "puntos_de_interes";

    protected $fillable = [
        'user_id', 'empresa_id', 'servicio_id', 'ubicacion', 'radio'
    ];
}
