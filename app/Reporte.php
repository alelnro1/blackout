<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reporte extends Model
{
    protected $table = "reportes";

    protected $fillable = [
        'user_id',
        'persona_id',
        'ubicacion',
        'radio',
        'servicio_id',
        'empresa_id',
        'resuelto'
    ];

    public function Reportes()
    {
        return $this->belongsToMany(Corte::class, 'corte_reporte');
    }

    public function getCantidadReportesDeCorte($corte_id)
    {
        return count($this->where('corte_id', $corte_id)->where('resuelto', '0')->get());
    }

    public static function getReportesParaInterseccion($nuevo_reporte){
        return static::where('servicio_id', $nuevo_reporte->servicio_id)->where('empresa_id', $nuevo_reporte->empresa_id)->where('id', '<>', $nuevo_reporte->id)->where('corte_id', null)->where('resuelto', 0)->get();
}


}
