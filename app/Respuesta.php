<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Respuesta extends Model
{
    protected $table = "respuestas";

    protected $fillable = [
        'corte_id',
        'descripcion',
        'user_id'
    ];

    public function Corte()
    {
        return $this->belongsTo(Corte::class, 'corte_id');
    }

    public function User()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
