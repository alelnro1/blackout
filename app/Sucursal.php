<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sucursal extends Model
{
    protected $table = "sucursales";

    protected $fillable = [
        'empresa_id', 'telefono', 'direccion', 'ubicacion'
    ];

    public function Empresa()
    {
        return $this->belongsTo(Empresa::class);
    }
}
