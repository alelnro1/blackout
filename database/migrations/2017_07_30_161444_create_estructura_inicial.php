<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstructuraInicial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('correo');
            $table->timestamps();
        });

        Schema::create('puntos_de_interes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('persona_id')->unsigned()->index();
            $table->integer('empresa_id')->unsigned()->index();
            $table->string('ubicacion')->string();
            $table->float('radio');
            $table->timestamps();
        });

        Schema::create('respuestas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('corte_id')->unsigned()->index();
            $table->integer('persona_id')->unsigned()->index();
            $table->integer('empresa_id')->unsigned()->index();
            $table->text('descripcion');
            $table->timestamps();
        });

        Schema::create('reportes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('persona_id')->unsigned()->index();
            $table->string('ubicacion');
            $table->float('radio');
            $table->integer('servicio_id')->unsigned()->index();
            $table->boolean('resuelto');
            $table->timestamps();
        });

        Schema::create('servicios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
        });

        Schema::create('corte_reporte', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('corte_id')->unsigned()->index();
            $table->integer('reporte_id')->unsigned()->index();
        });

        Schema::create('comentarios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('persona_id')->unsigned()->index();
            $table->integer('empresa_id')->unsigned()->index();
            $table->text('descripcion');
            $table->timestamps();
        });

        Schema::create('cortes_de_interes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('corte_id')->unsigned()->index();
            $table->integer('persona_id')->unsigned()->index();
            $table->integer('empresa_id')->unsigned()->index();
        });

        Schema::create('empresas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('password');
            $table->string('email');
            $table->string('telefono');
            $table->string('direccion');
            $table->string('website');
            $table->timestamps();
        });

        Schema::create('empresa_servicio', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('empresa_id')->unsigned()->index();
            $table->integer('servicio_id')->unsigned()->index();
        });

        Schema::create('cortes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('empresa_id')->unsigned()->index();
            $table->string('ubicacion');
            $table->float('radio');
            $table->integer('cantidad_reportes');
            $table->float('duracion');
            $table->boolean('resuelto');
            $table->dateTime('fecha_inicio');
            $table->dateTime('fecha_fin')->nullable();
            $table->timestamps();
        });

        Schema::create('sucursales', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('empresa_id')->unsigned()->index();
            $table->string('telefono');
            $table->string('direccion');
            $table->float('ubicacion');
        });

        /**
         * Creo las relaciones entre tablas
         */

        Schema::table('puntos_de_interes', function (Blueprint $table) {
            $table->foreign('persona_id')->references('id')->on('personas');
            $table->foreign('empresa_id')->references('id')->on('empresas');
        });

        Schema::table('respuestas', function (Blueprint $table) {
            $table->foreign('corte_id')->references('id')->on('cortes');
            $table->foreign('persona_id')->references('id')->on('personas');
            $table->foreign('empresa_id')->references('id')->on('empresas');
        });

        Schema::table('reportes', function (Blueprint $table) {
            $table->foreign('persona_id')->references('id')->on('personas');
            $table->foreign('servicio_id')->references('id')->on('servicios');
        });

        Schema::table('corte_reporte', function (Blueprint $table) {
            $table->foreign('corte_id')->references('id')->on('cortes');
            $table->foreign('reporte_id')->references('id')->on('reportes');
        });

        Schema::table('comentarios', function (Blueprint $table) {
            $table->foreign('persona_id')->references('id')->on('personas');
            $table->foreign('empresa_id')->references('id')->on('empresas');
        });

        Schema::table('cortes_de_interes', function (Blueprint $table) {
            $table->foreign('corte_id')->references('id')->on('cortes');
            $table->foreign('persona_id')->references('id')->on('personas');
            $table->foreign('empresa_id')->references('id')->on('empresas');
        });

        Schema::table('empresa_servicio', function (Blueprint $table) {
            $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->foreign('servicio_id')->references('id')->on('servicios');
        });

        Schema::table('cortes', function (Blueprint $table) {
            $table->foreign('empresa_id')->references('id')->on('empresas');
        });

        Schema::table('sucursales', function (Blueprint $table) {
            $table->foreign('empresa_id')->references('id')->on('empresas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('comentarios', function (Blueprint $table) {
            $table->dropForeign('comentarios_empresa_id_foreign');
            $table->dropForeign('comentarios_persona_id_foreign');
        });

        Schema::table('cortes', function (Blueprint $table) {
            $table->dropForeign('cortes_empresa_id_foreign');
        });

        Schema::table('cortes_de_interes', function (Blueprint $table) {
            $table->dropForeign('cortes_de_interes_corte_id_foreign');
            $table->dropForeign('cortes_de_interes_empresa_id_foreign');
            $table->dropForeign('cortes_de_interes_persona_id_foreign');
        });

        Schema::table('corte_reporte', function (Blueprint $table) {
            $table->dropForeign('corte_reporte_corte_id_foreign');
            $table->dropForeign('corte_reporte_reporte_id_foreign');
        });

        Schema::table('empresa_servicio', function (Blueprint $table) {
            $table->dropForeign('empresa_servicio_empresa_id_foreign');
            $table->dropForeign('empresa_servicio_servicio_id_foreign');
        });

        Schema::table('puntos_de_interes', function (Blueprint $table) {
            $table->dropForeign('puntos_de_interes_empresa_id_foreign');
            $table->dropForeign('puntos_de_interes_persona_id_foreign');
        });

        Schema::table('reportes', function (Blueprint $table) {
            $table->dropForeign('reportes_persona_id_foreign');
            $table->dropForeign('reportes_servicio_id_foreign');
        });

        Schema::table('respuestas', function (Blueprint $table) {
            $table->dropForeign('respuestas_corte_id_foreign');
            $table->dropForeign('respuestas_persona_id_foreign');
            $table->dropForeign('respuestas_empresa_id_foreign');
        });

        Schema::table('sucursales', function (Blueprint $table) {
            $table->dropForeign('sucursales_empresa_id_foreign');
        });

        Schema::dropIfExists('personas');
        Schema::dropIfExists('puntos_de_interes');
        Schema::dropIfExists('respuestas');
        Schema::dropIfExists('comentarios');
        Schema::dropIfExists('reportes');
        Schema::dropIfExists('servicios');
        Schema::dropIfExists('corte_reporte');
        Schema::dropIfExists('comentario');
        Schema::dropIfExists('cortes_de_interes');
        Schema::dropIfExists('empresas');
        Schema::dropIfExists('empresa_servicio');
        Schema::dropIfExists('cortes');
        Schema::dropIfExists('sucursales');
    }
}
