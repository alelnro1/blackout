<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarIdServicioPuntosDeInteres extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('puntos_de_interes', function (Blueprint $table) {
            $table->integer('servicio_id')->unsigned();
        });

        Schema::table('puntos_de_interes', function (Blueprint $table) {
            $table->foreign('servicio_id')->references('id')->on('servicios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('puntos_de_interes', function (Blueprint $table) {
            $table->dropForeign(['servicio_id']);
        });

        Schema::table('puntos_de_interes', function (Blueprint $table) {
            $table->dropColumn('servicio_id');
        });
    }
}
