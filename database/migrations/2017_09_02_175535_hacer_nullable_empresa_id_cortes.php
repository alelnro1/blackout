<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HacerNullableEmpresaIdCortes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cortes', function (Blueprint $table) {
            $table->dropForeign(['empresa_id']);
            $table->dropColumn(['empresa_id']);
        });

        Schema::table('cortes', function (Blueprint $table) {
            $table->integer('empresa_id')->unsigned()->nullable(true);
        });

        Schema::table('cortes', function (Blueprint $table) {
            $table->foreign('empresa_id')->references('id')->on('empresas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cortes', function (Blueprint $table) {
            $table->integer('empresa_id')->nullable(false)->change();
        });
    }
}
