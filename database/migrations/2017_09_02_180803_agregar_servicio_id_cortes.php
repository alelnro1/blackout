<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarServicioIdCortes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cortes', function (Blueprint $table) {
            $table->integer('servicio_id')->nullable()->unsigned();
        });

        Schema::table('cortes', function (Blueprint $table) {
            $table->foreign('servicio_id')->references('id')->on('cortes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cortes', function (Blueprint $table) {
            $table->dropForeign(['servicio_id']);
        });

        Schema::table('cortes', function (Blueprint $table) {
            $table->dropColumn(['servicio_id']);
        });
    }
}
