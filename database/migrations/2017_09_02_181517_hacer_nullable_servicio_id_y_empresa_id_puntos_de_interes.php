<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HacerNullableServicioIdYEmpresaIdPuntosDeInteres extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('puntos_de_interes', function (Blueprint $table) {
            $table->dropForeign(['empresa_id']);
            $table->dropForeign(['servicio_id']);

            $table->dropColumn(['empresa_id']);
            $table->dropColumn(['servicio_id']);
        });

        Schema::table('puntos_de_interes', function (Blueprint $table) {
            $table->integer('empresa_id')->unsigned()->nullable(true);
            $table->integer('servicio_id')->unsigned()->nullable(true);
        });

        Schema::table('puntos_de_interes', function (Blueprint $table) {
            $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->foreign('servicio_id')->references('id')->on('servicios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('puntos_de_interes', function (Blueprint $table) {
            $table->integer('empresa_id')->nullable(false)->change();
            $table->integer('servicio_id')->nullable(false)->change();
        });
    }
}
