<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CambiarPersonaIdPorUserIdCortesDeInteres extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cortes_de_interes', function (Blueprint $table) {
            $table->dropForeign(['persona_id']);
            $table->dropColumn(['persona_id']);
        });

        Schema::table('cortes_de_interes', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
        });

        Schema::table('cortes_de_interes', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cortes_de_interes', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropColumn(['user_id']);
        });

        Schema::table('cortes_de_interes', function (Blueprint $table) {
            $table->integer('persona_id')->unsigned();
        });

        Schema::table('cortes_de_interes', function (Blueprint $table) {
            $table->foreign('persona_id')->references('id')->on('personas');
        });
    }
}
