<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SacarEmpresaIdCortesDeInteres extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cortes_de_interes', function (Blueprint $table) {
            $table->dropForeign(['empresa_id']);
        });

        Schema::table('cortes_de_interes', function (Blueprint $table) {
            $table->dropColumn('empresa_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cortes_de_interes', function (Blueprint $table) {
            $table->integer('empresa_id')->unsigned();
        });

        Schema::table('cortes_de_interes', function (Blueprint $table) {
            $table->foreign('empresa_id')->references('id')->on('empresas');
        });
    }
}
