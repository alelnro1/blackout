<?php

use App\Servicio;
use Illuminate\Database\Seeder;

class ServiciosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Servicio::create(['nombre' => 'Agua']);
        Servicio::create(['nombre' => 'Luz']);
        Servicio::create(['nombre' => 'Gas']);
        Servicio::create(['nombre' => 'Cable']);
        Servicio::create(['nombre' => 'Teléfono']);
        Servicio::create(['nombre' => 'Internet']);
    }
}
