<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'Api\Auth\RegisterController@register');
Route::post('login', 'Api\Auth\LoginController@login');
Route::post('refresh', 'Api\Auth\LoginController@refresh');

// Registro de usuario-empresa
Route::post('register-empresa', 'Api\Auth\RegisterController@registerEmpresa');

Route::middleware('auth:api')->group(function() {
    Route::post('logout', 'Api\Auth\LoginController@logout');

    Route::group(['namespace' => 'Api'], function () {
        Route::get('/user', 'UsuariosController@mostrarUsuario');
        Route::get('/user/deshabilitar', 'UsuariosController@deshabilitar');
        Route::get('/usuarios', 'UsuariosController@index');

        Route::post('/user/actualizar', 'UsuariosController@actualizar');

        Route::get('{empresa}/sucursales', 'SucursalesController@getSucursalesDeEmpresa');
        Route::get('usuarios/{usuario}/puntos-de-interes', 'PuntosDeInteresController@getPuntosDeInteresDeUsuario');
        Route::get('usuarios/{usuario}/reportes', 'ReportesController@getReportesDeUsuario');
        Route::get('usuarios/{usuario}/cortes-de-interes', 'CortesDeInteresController@getCortesDeInteresDeUsuario');

        Route::resource('empresa', 'EmpresasController', ['except' => 'update']);
        Route::post('empresa/{empresa}/actualizar', 'EmpresasController@update');
        Route::get('empresa/{empresa}/sucursales', 'SucursalesController@getSucursalesDeEmpresa');
        Route::get('empresa/{empresa}/comentarios', 'ComentariosController@getComentariosDeEmpresa');
        Route::get('empresa/{empresa}/cortes', 'CortesController@getCortesDeEmpresa');
        Route::get('empresa/{empresa}/habilitar', 'EmpresasController@habilitarEmpresa');
        Route::get('empresa/{empresa}/deshabilitar', 'EmpresasController@deshabilitarEmpresa');

        Route::post('empresa/{empresa}/asociar-usuario', 'UsuariosController@asociarEmpresaConUsuario');

        Route::group(['prefix' => 'reporte'], function () {
            Route::get('/', 'ReportesController@index');
            Route::post('/', 'ReportesController@store');
            Route::post('/{reporte}/resolver', 'ReportesController@resolver');
            Route::delete('/{reporte}/delete', 'ReportesController@destroy');

            Route::get('{reporte}/actualizar-updated-at', 'ReportesController@actualizarUpdatedAt');
        });

        Route::group(['prefix' => 'cortes-de-interes'], function () {
            Route::post('/', 'CortesDeInteresController@store');
            Route::delete('{corte}', 'CortesDeInteresController@destroy');
        });

        Route::group(['prefix' => 'persona'], function () {
            Route::post('/', 'PersonasController@store');
            Route::get('/{persona}', 'PersonasController@show');
            Route::delete('/{persona}/delete', 'PersonasController@destroy');
        });

        Route::group(['prefix' => 'punto-de-interes'], function () {
            Route::post('/', 'PuntosDeInteresController@store');
            Route::get('{punto}', 'PuntosDeInteresController@show');
            Route::delete('/{punto}/delete', 'PuntosDeInteresController@destroy');
        });

        Route::group(['prefix' => 'comentario'], function () {
            Route::post('/', 'ComentariosController@agregar');
            Route::get('{comentario}', 'ComentariosController@show');
            Route::delete('/{comentario}/delete', 'ComentariosController@destroy');
        });

        Route::group(['prefix' => 'respuestas'], function () {
            Route::post('/', 'RespuestasController@agregar');
            Route::get('{respuesta}', 'RespuestasController@show');
            Route::delete('/{respuesta}/delete', 'RespuestasController@destroy');
        });

        Route::group(['prefix' => 'sucursal'], function () {
            Route::post('/', 'SucursalesController@store');
            Route::get('{sucursal}', 'SucursalesController@show');
            Route::delete('/{sucursal}/delete', 'SucursalesController@destroy');
        });

        Route::group(['prefix' => 'corte-programado'], function () {
            Route::post('/', 'CortesController@altaCorteProgramado');
            Route::post('{corte}/resolver', 'CortesController@resolverCorteProgramado');
            Route::delete('{corte}', 'CortesController@destroyCorteProgramado');
        });

        Route::group(['prefix' => 'cortes'], function () {
            Route::get('/', 'CortesController@index');
            Route::get('{corte}/respuestas', 'CortesController@getRespuestasDeCorte');
        });
    });
});